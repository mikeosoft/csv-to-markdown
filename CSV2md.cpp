#include <iostream>
#include <fstream>
#include <string>

int main (int argc, char *argv[])
{
    std::cerr << "CSV2md\nConverts a CSV (Comma Separated Variable) file to Markdown Format!\nWritten by Mike O'Shea - Version 1.00\n" << std::endl;
    int retval(0);
    
    std::string in_file_name;
    if(argc > 1) {  // check the command line for source CSV file
      in_file_name = argv[1]; // If a value has been passed on the command line use it.
    } else { // If nothing has been passed on the command line, prompt for the file name
      char temp[1024];	// to hold input file name
      std::cout << "Enter the name of the CSV file: ";
      std::cin.get (temp, 1024);    // get char array
      in_file_name = temp;
    }
    std::ifstream input_file(in_file_name.c_str()); // open file
    if(input_file.good()) {
        bool withinquotes(false);  // Indicates state of between two quotation marks.
        char replace = ',';	   // Character that should be replaced.
        std::string	output;	   // string to hold output
        char c;		           // Holder for character read from file
	bool header_done(false);   // Indicates if the line required after the header has been added.
        int col_count(1);	// Starts at 1 because incremenation takes place after the opperation
	
        while (input_file.get(c)) { // loop reading a single character from the file at a time
	    if(c == '\"') { // If it's a quotation mark switch the value of the bool to its opersite state
	      withinquotes = !withinquotes;
	    }  
            if((c == replace) && (!withinquotes)) {   // If it is the specific character and it's not within quotes
                c = '|';
                if(!header_done) {  // only for the first row, count the number of columns
                    ++col_count;
                }
            }
            if(c != '\r') {   // converts from Windows line ending to linux by skipping this character
                output += c;
            }
            if((c == '\n') && (!header_done)) {  // If header line is not added, add it.
	      for(int i = 0; i < col_count; ++i) { // Add a column header for each column
		output += " --- |";
	      }
	      output += '\n';
	      header_done = true; // Mark the header as added.
            }
        }
        input_file.close();  // close the input file - not required as it will go out of scope.

        std::string out_file_name(in_file_name.substr(0, in_file_name.rfind(".")));// Create name for output file
	out_file_name += ".md"; 
        if(output.size() > 0) {  // If the string holds a value write it to a file
            std::ofstream output_file(out_file_name.c_str()); // Open the output file
	    if(output_file.good()) {
	      output_file << output.c_str() ;
	      output_file.close(); // Close the output file - not required as it will go out of scope.
	    } else {
	      std::cerr << "The output File: " << out_file_name.c_str() << " could not be opened!\n" <<  std::endl;
	      retval = 2;
	    }
        }
        std::cout << "Output written to: " << out_file_name.c_str() << "\n" << std::endl;
    } else {
        std::cerr << "The input File: " << in_file_name.c_str() << " could not be found/opened!\n" <<  std::endl;
	retval = 1;
    }
    return retval;
}
