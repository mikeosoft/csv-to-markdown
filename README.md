# CSV to Markdown
##CSV2md by Mike O'Shea
This Program was written because I needed to convert some data, stored in spreadsheets into Markdown format.  It is a fairly simple command line tool, written using the C++ Standard Library and so should compile on any system, with an appropriate compiler.  The code is fairly simple and is a quick solution for the problem I had, that I felt might be of use to others.  The code has basic error checking.

Spreadsheets are good tools for working with tabular data; as they give you the ability to sort on columns, spell check and to manipulate the data easily.

Spreadsheets programs tend to have the ability to save Worksheets in CSV (Comma Separated Variable) format, which is a plain text format, so it contains no formulas or formatting or anything other than human readable text.

CSV format is often used for transferring data between different programs, without having problems with the incompatibly of binary file formats.  Because it is plain text it is readable within a wide variety of programs.

The Comma in the CSV is the delimiter that separates each column in the spreadsheet when it is saved in a file.  The end of line character separates each row.  A problem with CSV format is that the comma is commonly used punctuation and so is likely to appear within text stored within the spreadsheet.  Some programs give you the option, when saving to CSV, of saving text within quotation marks. When this program processes the text within the file, it doesn't replace commas that are within quotation marks, so the text will remain intact.  The output file created by this program could also be referred to as a pipe delimited file.
Some programs will let you save you data with different specific delimiters such as tabs or space and others will let you specify a delimiter you choose.  Libra Office will let you do this; the current version of Microsoft Office doesn’t seem to, unless you adjust the configuration, which would have wider impact.

An alternative way to achieve something similar to what this program achieves is to load your spreadsheet into Libra Office and choosing to save it in CSV format.  As you go through the steps to achieve this, you are given the opportunity to choose the delimiter you use, if you choose the pipe character '|' you will almost achieve what this program will do for you.  To make it the same you would then need to add the row that makes the title row, a title row that becomes the second row in the table. 

The program will only work if the spreadsheet starts with the title row of the table, it shouldn't have a title or anything else other than the table.

To use this program you need to save your table in CSV format, putting it in the same folder as the program.  When you execute the program on the command line you will be prompted for the file name.  The program will then process the file, producing a file with the same file name as the CSV, but with the ‘md’ extension instead of the CSV.

Markdown, which is what this program generates for its output, is a very simple way of adding formatting like headers, bold, bulleted lists and tables to plain text files.  It is like a simpler form of HTML, that is read by an editor program which can be used to generate output files from it that include the formatting, such as full html or PDF files.  Markdown files can be edited in specifically written editors such as [ReText](https://github.com/retext-project/retext "Link to ReText download site") or conventional text editors such as Notepad, Notepad++, vi, EMACs and Kate.

The program will produce a file with Linux end of line characters.  If you are working on Windows all you would need to do to convert it back to Windows end of lines is to load the file into WordPad and resave it and it will add the additional character, or change my code before you compile it, removing the lines that remove the '\r' characters.

To compile the program you need to have the GNU C++ compiler installed on your system and to execute the command below:

     G++ -o CSV2md CSV2md.cpp
     
This compiler is freely available on all systems, though the code should compile with any C++ compiler.

This will produce an executable file called: CSV2md that you can execute on the command line. 

You can either enter the name of your CSV file on the command line, or if you leave the line clear you will be prompted for input.

This code is really too simple to have a licence, but for the sake of completeness consider in licensed under the [GNU GPL licence V3.0](https://github.com/retext-project/retext "Link to GNU Lincence text").



